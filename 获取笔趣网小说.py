# coding:utf-8
import traceback
import requests
import os
from threading import Thread
from bs4 import BeautifulSoup

req_header = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Cookie': 'clickbids=13655; Hm_lvt_ab3c0de16bd108959c9d601fa1f6967e=1585204503,1585204930; Hm_lpvt_ab3c0de16bd108959c9d601fa1f6967e=1585211904',
    'Host': 'www.vipzw.com',
    'Proxy-Connection': 'keep-alive',
    'Referer': 'http://www.vipzw.com/',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'
}

req_url_base = 'http://www.vipzw.com/'  # 小说主地址


def get_txt(txt_id):
    txt = {}
    txt['title'] = ''
    txt['id'] = str(txt_id)
    try:
        # print("请输入需要下载的小说编号：")
        # txt['id']=input()
        req_url = req_url_base + txt['id'] + '/'  # 根据小说编号获取小说URL
        print("小说编号：" + txt['id'])
        res = requests.get(req_url, params=req_header)  # 获取小说目录界面
        res_response = bytes(res.text, res.encoding).decode('utf-8', 'ignore')
        soups = BeautifulSoup(res_response, "html.parser")  # soup转化
        # 获取小说题目
        txt['title'] = soups.select('#wrapper .box_con #maininfo #info h1')[0].text
        txt['author'] = soups.select('#wrapper .box_con #maininfo #info p')
        txt['author'] = txt['author'][0].text
        # 获取小说简介
        print("编号：" + '{0:0>8}   '.format(txt['id']) + "小说名：《" + txt['title'] + "》  开始下载。")
        print("正在获取所有章节地址。。。")
        all_page_address = soups.select('#wrapper .box_con #list dl dd a')[9:]
        new_l = []
        for i in all_page_address:
            if '章' in str(i):
                new_l.append(i)

        # 获取小说总章页面数
        section_ct = len(new_l)
        print(f'一共章节数目：{len(new_l)}')
        print(new_l)
        for one in new_l:
            print(one)
            url = str(one).split('<a href="')[1].split('">')[0]
            title_name = str(one).split('">')[1].split('</a>')[0]
            if not os.path.exists('book'):
                os.mkdir('book')
            book_path = os.path.join('book', f"{txt['id']}_{txt['title']}.txt")
            with open(book_path, 'a', encoding='utf-8') as f:
                content_url = req_url_base + url  # 根据小说编号获取小说URL
                content_res = requests.get(content_url, params=req_header)  # 获取小说目录界面
                content_response = bytes(content_res.text, content_res.encoding).decode('utf-8', 'ignore')
                content_soups = BeautifulSoup(content_response, 'html.parser')
                f.write('\n               ' + title_name + '\n\n')
                content_txt = content_soups.find('div', id='content').get_text().split()[:-3]
                for txt_one in content_txt:
                    f.write(' ' + str(txt_one) + '\n')

        # txt['title'] = soups.select('#wrapper .box_con #maininfo #info h1')[0].text
        print("小说章节页数：" + str(section_ct))
        # 打开小说文件写入小说相关信息
    except Exception as e:
        traceback.print_exc()


def get_all_book_id():
    book_id_url_base = 'http://www.vipzw.com/map/1.html'
    book_id_res = requests.get(book_id_url_base, params=req_header)  # 获取小说目录界面
    book_id_res_response = bytes(book_id_res.text, book_id_res.encoding).decode('utf-8', 'ignore')
    book_id_soups = BeautifulSoup(book_id_res_response, "html.parser")
    book_id_content_txt = book_id_soups.findAll('li')
    book_id_list = []
    for book_id_ in book_id_content_txt:
        book_id_list.append(str(book_id_).split('<li><a href="http://www.vipzw.com/')[1].split('/">')[0])


def start_thread(book_id_l_name):
    for book_one in book_id_l_name:
        t = Thread(target=get_txt, args=(book_one,))
        t.start()


if __name__ == "__main__":
    print('============小说对应的网站是：http://www.vipzw.com/ ==============')
    book_id = input('请输入小说编号：')
    get_txt(book_id)
